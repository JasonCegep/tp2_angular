import { custom, JSONObject, optional, required } from "ts-json-object";

export class Recipe extends JSONObject {
  @optional
  id!: string;

  @required
  category!: string;

  @required
  name!: string;

  @optional
  description!: string;

}
