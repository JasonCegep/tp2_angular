import { custom, JSONObject, optional, required } from "ts-json-object";
import { Recipe } from "./recipe.model";

export class User extends JSONObject {
  @required
  @custom((user: User, key:string, value: string) => {
    return value.toLowerCase();
  })
  email!: string;

}
