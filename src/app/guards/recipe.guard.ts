import { Injectable } from '@angular/core';
import { CanActivateChild, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { RecipeService } from '../services/recipe.service';

@Injectable({
  providedIn: 'root'
})


export class RecipeGuard implements CanActivateChild {

  constructor(private recipeService: RecipeService, private route: Router){}

  canActivateChild(
    childRoute: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

      let id = childRoute.paramMap.get('id')!;

      return this.recipeService.getRecipe(id).pipe(
        map(recipe => {
          if(recipe)
          {
            return true;
          }
          else {
            return this.route.parseUrl('/recipes');
          }

        })

      );


   }

}
