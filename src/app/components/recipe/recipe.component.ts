import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Recipe } from 'src/app/models/recipe.model';
import { RecipeService } from 'src/app/services/recipe.service';

@Component({
  selector: 'app-recipe',
  templateUrl: './recipe.component.html',
  styleUrls: ['./recipe.component.css']
})
export class RecipeComponent implements OnInit {

  id!: String;
  recipe!: Recipe;

  constructor(private route: ActivatedRoute, private recipeService: RecipeService, private router: Router) {
    this.id = this.route.snapshot.paramMap.get('id')!;

     this.recipeService.getRecipe(this.id).subscribe(
      recipe => {
        if(recipe) {
          this.recipe = recipe;
        }
      }
    )

  }

  ngOnInit(): void {
  }

  formDone(recipeForm: any) {

    this.recipeService.updateRecipe(recipeForm).subscribe(success => {
      if (success) {
        this.router.navigate(['/recipes']);
      } else {
        alert("Failed to update recipe");
      }
    });

  }



}
