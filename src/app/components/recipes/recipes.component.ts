import { Component, OnInit } from '@angular/core';
import { FormControl, FormControlDirective, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { AuthService } from 'src/app/services/auth.service';
import { RecipeService } from 'src/app/services/recipe.service';
import { Recipe } from '../../models/recipe.model';

@Component({
  selector: 'app-recipes',
  templateUrl: './recipes.component.html',
  styleUrls: ['./recipes.component.css']
})
export class RecipesComponent implements OnInit {


  recipes!: Recipe[];

  constructor(private authService: AuthService, private recipeService: RecipeService, private router: Router) { }

  ngOnInit(): void {
  this.recipes = [];
  this.getRecipes();

  }

  formDone(recipeForm: any) {


      this.recipeService.addRecipe(recipeForm).subscribe(success => {
        if (success) {
          this.getRecipes();
        } else {
          alert("Failed to load recipes");
        }
      });



  }

  getRecipes(){
    this.recipeService.getRecipes().pipe(

      map(data => {

        if(data) {
          this.recipes = (data as Recipe[]).map(obj =>new Recipe(obj));
          return true;
        }
        else {
          return false;
        }

      })

    ).subscribe(data => {
      if (data) {

      } else {
        alert("Failed to load recipes");
      }
    });
  }

  recipeResponse(json: any){
      if(json.type == "delete") {
        this.getRecipes();
      }
      else {
        console.log("navigating...");
         this.router.navigate(['/recipe', json.recipe.id]);

      }
  }


}
