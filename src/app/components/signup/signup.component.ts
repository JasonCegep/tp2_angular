import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormControlName, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from 'src/app/services/auth.service';
import { UserCredentials } from '../../models/user-credentials.model';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  signupForm: FormGroup;

  constructor(private authService: AuthService, private router: Router, translate: TranslateService) {
    this.signupForm = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required, Validators.minLength(3)]),
      passwordConfirm: new FormControl('', [Validators.required])
    }, [this.passwordMatch]);

   }

   private passwordMatch(form: AbstractControl): ValidationErrors | null {
    if (form.get('password')?.value != form.get('passwordConfirm')?.value) {
      return { passwordConfirmationMustMatch: true };
    } else {
      return null
    }
  }

  ngOnInit(): void {
  }

  signUp() {

    this.authService.signUp(new UserCredentials(this.signupForm.value)).subscribe(success => {

      if (success) {
        this.router.navigate(['/recipes']);
      } else {
        alert("Invalid Credentials!");
      }
    });



  }

}
