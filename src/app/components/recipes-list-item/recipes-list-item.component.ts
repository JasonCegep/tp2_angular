import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Recipe } from '../../models/recipe.model';

@Component({
  selector: '[app-recipes-list-item]',
  templateUrl: './recipes-list-item.component.html',
  styleUrls: ['./recipes-list-item.component.css']
})
export class RecipesListItemComponent implements OnInit {

  @Input() recipe!: Recipe;
  @Output() recipeClicked = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  clicked(param: String) {

    let params = {
      recipe: this.recipe,
      type: param
    }


    this.recipeClicked.emit(params);


  }


}
