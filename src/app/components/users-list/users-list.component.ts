import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { User } from '../../models/user.model';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.css']
})
export class UsersListComponent implements OnInit {

  get users(): User[] {
    return this.authService.users;
  }

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit(): void {
  }

  userClicked(userEmail: any) {
    this.router.navigate(['/profile', userEmail]);
  }

}
