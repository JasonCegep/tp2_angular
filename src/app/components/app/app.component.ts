import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from 'src/app/services/auth.service';
import { RecipeService } from 'src/app/services/recipe.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})


export class AppComponent {
  title = 'Recipeasy';

  lang!: String;

  constructor(public authService: AuthService, private recipeService: RecipeService, private translate: TranslateService) {
        recipeService.seed();

        translate.addLangs(['en', 'fr']);
        translate.setDefaultLang('en');

        var a = localStorage.getItem('lang');

        if(a && a == "fr") {
          translate.use(a);
        }
        else {
          localStorage.setItem('lang', 'en');
          translate.use('en');
        }

  }

  click(lang: String) {

    console.log(lang);

    if(lang == "fr") {
      localStorage.setItem('lang', 'fr');
      this.translate.use('fr');
    }
    else {
      localStorage.setItem('lang', 'en');
      this.translate.use('en');

    }
  }



  logout()
  {
    this.authService.logOut();
  }

  get isConnected(): boolean {
      return this.authService.isLoggedIn;
  }


}
