import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Recipe } from 'src/app/models/recipe.model';


@Component({
  selector: 'app-recipes-form',
  templateUrl: './recipes-form.component.html',
  styleUrls: ['./recipes-form.component.css']
})
export class RecipesFormComponent implements OnInit, OnChanges {

  recipeForm: FormGroup;
  @Output() formDoneEvent =  new EventEmitter();
  @Input() recipe: Recipe | null = null;

  constructor() {

    this.recipeForm = new FormGroup({
      id: new FormControl(),
      name: new FormControl('', [Validators.required, Validators.maxLength(15)]),
      description: new FormControl(''),
      category: new FormControl('', [Validators.required])
    });



  }
  ngOnChanges(changes: SimpleChanges): void {

    if(this.recipe) {
      console.log(this.recipe);
      this.recipeForm.setValue(this.recipe);
    }

  }


  ngOnInit(): void {


  }

  recipeFormDone() {
    this.formDoneEvent.emit(this.recipeForm.value);
  }

}
