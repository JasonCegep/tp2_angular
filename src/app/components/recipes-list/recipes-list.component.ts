import { stringify } from '@angular/compiler/src/util';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { map } from 'rxjs/operators';
import { AuthService } from 'src/app/services/auth.service';
import { RecipeService } from 'src/app/services/recipe.service';
import { Recipe } from '../../models/recipe.model';

@Component({
  selector: 'app-recipes-list',
  templateUrl: './recipes-list.component.html',
  styleUrls: ['./recipes-list.component.css']
})
export class RecipesListComponent implements OnInit {

  @Input() recipeList!: Recipe[];
  @Output() recipeClick = new EventEmitter();


  constructor(private recipeService: RecipeService, private authService: AuthService) {  }

  ngOnInit(): void {
  }



  recipeClicked(json: any) {

    if(json.type == "delete")
    {
      this.recipeService.deleteRecipe(json.recipe).subscribe(
        success => {
          if(success)
          {
            this.recipeClick.emit(json);
          }
          else
          {
            alert("hmmm...");
          }
        }
      );
    }
    else
    {
      this.recipeClick.emit(json);
    }


  }

}
