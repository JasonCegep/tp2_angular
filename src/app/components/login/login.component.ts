import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { UserCredentials } from '../../models/user-credentials.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public loginForm: FormGroup;


  constructor(private authService: AuthService, private router: Router) {
      this.loginForm = new FormGroup({
        email: new FormControl('', [Validators.required, Validators.email]),
        password: new FormControl('', [Validators.required, Validators.minLength(3)])
      });

   }


  ngOnInit(): void {
  }

  logIn(){

    this.authService.logIn(this.loginForm.value).subscribe(success => {
      if (success) {
        this.router.navigate(['/recipes']);
      } else {
        alert('Invalid Credentials');
      }
    });

  }

}
