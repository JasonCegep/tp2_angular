import { Routes } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { ProfileDetailsComponent } from './components/profile-details/profile-details.component';
import { ProfileComponent } from './components/profile/profile.component';
import { RecipeComponent } from './components/recipe/recipe.component';
import { RecipesComponent } from './components/recipes/recipes.component';
import { SignupComponent } from './components/signup/signup.component';
import { AuthGuard } from './guards/auth.guard';
import { LoginGuard } from './guards/login.guard';
import { RecipeGuard } from './guards/recipe.guard';

export const routes: Routes = [

  {path: '', canActivateChild: [LoginGuard], children: [
    { path: '', component: LoginComponent},
    { path: 'signup', component: SignupComponent},
  ]},

  {path: '', canActivateChild: [AuthGuard], children: [
    { path: 'recipes', component: RecipesComponent},

    {path: '', canActivateChild: [RecipeGuard], children: [
      { path: 'recipe/:id', component: RecipeComponent},
    ]},


    { path: 'profile', component: ProfileComponent},
    { path: 'profile/:email', component: ProfileDetailsComponent}
  ]},

  { path: '**', component: PageNotFoundComponent }


];
