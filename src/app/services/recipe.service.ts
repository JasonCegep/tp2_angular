import { Injectable } from "@angular/core";
import { FormGroup } from "@angular/forms";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { Recipe } from "../models/recipe.model";
import { UserCredentials } from "../models/user-credentials.model";
import { User } from "../models/user.model";
import { AuthService } from "./auth.service";
import { MarthaRequestService } from "./martha-request.service";

@Injectable({
  providedIn: 'root'
})
export class RecipeService {

  constructor(private authService: AuthService, private martha: MarthaRequestService) {
  }



getRecipes() {

  let credentials = this.authService.currentUser;

  return this.martha.select('recipes-list', credentials);

}

get recipes() {

  let recipes : Recipe[] = [];
  let storedRecipes = JSON.parse(localStorage.getItem('recipeasy.recipes') ?? 'null');

  if (storedRecipes) {
    recipes = (storedRecipes as Recipe[]).map(obj => new Recipe(obj));
  }

  return recipes

}



getRecipe(id: String): Observable<Recipe | null>  {

  let email = this.authService.currentUser!.email;
  return this.martha.select('recipes-read', {id, email}).pipe(
    map(data => {
      if(data && data.length == 1) {
        return new Recipe(data[0]);
      } else {
        return null;
      }
    })
  );

}

updateRecipe(recipeForm: any) {
  let recipe = new Recipe(recipeForm);
  let userEmail = this.authService.currentUser!.email;

  return this.martha.insert('recipes-update', {...recipe, userEmail}).pipe(
    map(result => {
      if(result.success) {
        return true;
      }
      else {
        return false;
      }

    })
  );

}

addRecipe(recipeForm: any) {


  let recipe = new Recipe(recipeForm);
  recipe.id = Math.random().toString(16).substring(2)
  let userEmail = this.authService.currentUser!.email;

  return this.martha.insert('recipes-create', {...recipe, userEmail}).pipe(
    map(result => {
      if(result.success) {
        return true;
      }
      else {
        return false;
      }

    })
  );


}

deleteRecipe(recipe: Recipe) {


  return this.martha.insert('recipes-delete', recipe).pipe(
    map(result => {
      if(result.success) {
        return true;
      }
      else {
        return false;
      }

    })
  );


}

seed() {
  if(localStorage.length == 0)
  {

    let recipes : Recipe[] = [];
    recipes.push(new Recipe({
      id: Math.random().toString(16).substring(2),
      name: "Recipe Name",
      description: "Big description",
      category: "lunch",
      userEmail: "b@b"
    }));

    recipes.push(new Recipe({
      id: Math.random().toString(16).substring(2),
      name: "Name",
      description: "description",
      category: "diner",
      userEmail: "b@b"
    }));

    recipes.push(new Recipe({
      id: Math.random().toString(16).substring(2),
      name: "Example Name",
      description: "Example description",
      category: "breakfast",
      userEmail: "b@b"
    }));

    recipes.push(new Recipe({
      id: Math.random().toString(16).substring(2),
      name: "Recipe Name",
      description: "Big description",
      category: "lunch",
      userEmail: "c@c"
    }));

    localStorage.setItem('recipeasy.recipes', JSON.stringify(recipes));

    this.authService.seed();

  }

}



}
