import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { InsertResult } from '../models/insert-result.model';
import { UserCredentials } from '../models/user-credentials.model';
import { map, catchError, tap } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class MarthaRequestService {
  private readonly username = 'gelinas';
  private readonly password = 'bi8wa3n5';

  constructor(private http: HttpClient) { }

  private get headers() {
    return { headers: {'auth' : btoa(`${this.username}:${this.password}`)}};
  }

  private getUrl(query: string) {
    return `http://martha.jh.shawinigan.info/queries/${query}/execute`;
  }

  select(query: string, body: any = null): Observable<any> {
    return this.http.post<any>(this.getUrl(query), body, this.headers).pipe(

      map(response => {

        if (response.success) {
          return response.data;
        } else {
          return false;
        }
      }),

      catchError(error => {
        console.log('Error', error);
        return of(null);
      })

    );
  }

  insert(query: string, body: any = null): Observable<any> {
    return this.http.post(this.getUrl(query), body, this.headers).pipe(
      map(result => {
        return new InsertResult(result);
      }),
      catchError(error => {
        console.log('Error', error);

        return of(null);
      })
    );
  }
}
