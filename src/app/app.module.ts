import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClient, HttpClientModule } from "@angular/common/http";
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './components/app/app.component';
import { LoginComponent } from './components/login/login.component';
import { SignupComponent } from './components/signup/signup.component';
import { ProfileComponent } from './components/profile/profile.component';
import { UsersListComponent } from './components/users-list/users-list.component';
import { UsersListItemComponent } from './components/users-list-item/users-list-item.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { RecipesComponent } from './components/recipes/recipes.component';
import { RecipesListComponent } from './components/recipes-list/recipes-list.component';
import { RecipesListItemComponent } from './components/recipes-list-item/recipes-list-item.component';
import { ProfileDetailsComponent } from './components/profile-details/profile-details.component';
import { RecipesFormComponent } from './components/recipes-form/recipes-form.component';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { RecipeComponent } from './components/recipe/recipe.component';



export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    SignupComponent,
    ProfileComponent,
    UsersListComponent,
    UsersListItemComponent,
    PageNotFoundComponent,
    RecipesComponent,
    RecipesListComponent,
    RecipesListItemComponent,
    ProfileDetailsComponent,
    RecipesFormComponent,
    RecipeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
         useFactory: createTranslateLoader,
         deps: [HttpClient]
      }
    }),
    BrowserAnimationsModule,
    BsDropdownModule.forRoot()


  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
